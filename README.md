


# Scratch experiments 

This repo provides scratch examples I primarly made for teaching reasons. I do primarly work with the [offline editor](https://scratch.mit.edu/download).

| Projects | Description |
| -------- | -------- |
| [Flappy Bird Clone](https://git.snopyta.org/plumps/scratch_experiments/src/branch/master/flappy_bird_game)|You control a character with your mouse, but avoid contacting the pipes.| 